const createElement = (type, attributes = {}, children) => {
	const element = document.createElement(type);

	if (Object.keys(attributes).length > 0) {
		Object.keys(attributes).map(key => {
			element.setAttribute(key, attributes[key].toString());
		})
	}
	
	if(typeof children === "undefined") {
		return element;
	}
	
	if(Array.isArray(children)) {
		
		children.forEach(child => {
			
			if(child === null) {
				return false;
			}
			
			if(typeof child === "string") {
				const text = document.createTextNode(child);
				element.appendChild(text);
			} else if ( child instanceof HTMLElement ) {
				element.appendChild(child);
			} else {
				throw new Error(`Unresolved type ${typeof child},${child}`)
			}
		})
		
	} else {
		
		if(children === null) {
			return false;
		}
		
		if(typeof children === "string") {
			const text = document.createTextNode(children);
			element.appendChild(text);
		} else if ( children instanceof HTMLElement ) {
			element.appendChild(children);
		} else {
			throw new Error(`Unresolved type [${typeof children}]`)
		}
	}
	
	return element;
};

export default createElement;