import createElement from "./createElement";

export function addingMessage(type, link, message) {
	const prevMessage = document.querySelector(".alert");
	if(prevMessage) {
		prevMessage.parentNode.removeChild(prevMessage);
	}
    console.log(type);
    const root = createElement(
		"a",
		{
			class: `alert ${type}`,
			href: link
		},
		[
			// createElement(
			// 	"img",
			// 	{
			// 		src: "/images/wish-added-icon.svg",
			// 		class: "c-snackbar__icon",
			// 		alt: "",
			// 	}),
			createElement(
				"h3",
				{
					class: "c-text c-text--h5 a-color-light",
				},
				// window.__i18n__.addToWishlist\
                message
			),
			// createElement(
			// 	"a",
			// 	{
			// 		class: "c-icon c-icon--arrow-right a-color-light-t4",
			// 	}
			// ),
		]
	);

	document.body.appendChild(root);

	setTimeout(() => {
		if(root && root.parentNode) {
			root.parentNode.removeChild(root);
		}
	}, 2000)
}
