import Vue from 'vue'
import App from './App.vue'
import router from "./router/router";
import vueResource from 'vue-resource';
import store from "./vuex/store";
import Paginate from 'vuejs-paginate'


Vue.use(vueResource);
Vue.component('paginate', Paginate)


new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
