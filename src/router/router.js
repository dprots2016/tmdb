import Vue from 'vue'
import VueRouter from 'vue-router'
import filmsPage from '../components/filmsPage.vue'
import filmPage from '../components/filmPage.vue'
import homePage from '../components/homePage'
import serialsPage from '../components/serialsPage'
import personsPage from "../components/personsPage";
import personPage from "../components/personPage"
import searchPage from "../components/searchPage";
import favoritesPage from "../components/favoritesPage";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'homePage',
    component: homePage,
    props: true
  },
  {
    path: '/films',
    name: 'filmsPage',
    component: filmsPage
  },
  {
    path: '/serials',
    name: 'serialsPage',
    component: serialsPage
  },
  {
    path: '/film/:id',
    name: 'filmPage',
    component: filmPage,
    props: true,

  },
  {
    path: '/persons',
    name: 'personsPage',
    component: personsPage

  },
  {
    path: '/person/:id',
    name: 'personPage',
    component: personPage,
    props: true,

  },
  {
    path: '/search',
    name: 'searchPage',
    component: searchPage,
    props: true,

  },
  {
    path: '/favorites',
    name: 'favoritesPage',
    component: favoritesPage,
    props: true,
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
