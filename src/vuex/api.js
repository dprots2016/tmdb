import axios from "axios";
import constants from "../constants";

export default {
    getPersonFromApi() {
        return axios.get(`${constants.baseUrl}/person/popular?api_key=${constants.apiKey}&language=uk`)
    }
}
