import Vue from 'vue';
import Vuex from 'vuex';
import axios from "axios";
import api from "./api";
import constants from "../constants";


Vue.use(Vuex)

export default new Vuex.Store({
    actions: {
        GET_POPULAR_PERSONS_FROM_API(context, page) {
            axios.get(`${constants.baseUrl}/person/popular?api_key=${constants.apiKey}&language=uk&page=${page || 1}`)
                .then((response) => {
                    context.commit('SET_POPULAR_PERSONS_TO_VUEX', response.data)
                });
        },
        GET_PERSON_FROM_API({commit}) {
            api.getPersonFromApi()
                .then((response) => {
                    commit('SET_PERSON_TO_VUEX', response.data)
                })
        },
        GET_POPULAR_FILMS_FROM_API(context, type) {
            axios.get(`${constants.baseUrl}/${type}/popular?api_key=${constants.apiKey}&language=uk&page=${this.state.page}`)
                .then((response) => {
                    response.data.results.forEach( (item) => {
                        item.media_type = type
                    })
                    context.commit('SET_POPULAR_FILMS_TO_VUEX', response.data);
                })
        },
        GET_TRENDING_MOVIES_FROM_API(context, time) {
            axios.get(constants.baseUrl + `/trending/all/${time}?api_key=${constants.apiKey}`)
                .then((response) => {
                    context.commit('SET_TRENDING_MOVIES_TO_VUEX', response.data)
                })
        },
        GET_UPCOMING_FILMS_FROM_API(context) {
            axios.get(constants.baseUrl + '/movie/upcoming?api_key=' + constants.apiKey + '&language=uk&page=' + this.state.page)
                .then((response) => {
                    context.commit('SET_UPCOMING_FILMS_TO_VUEX', response.data);
                    response.data.results.forEach((item) => {
                        if (!context.state.upcomingFilmsIds.includes(item.id)) {
                            context.state.upcomingFilmsIds.push(item.id);
                        }
                    })
                    context.state.upcomingFilmsIds.forEach((id) => {
                        axios.get(constants.baseUrl + `/movie/${id}/videos?api_key=` + constants.apiKey + '&language=uk')
                            .then((response) => {
                                for (let i = 0; i < response.data.results.length; i++) {
                                    if (response.data.results[i]) {
                                        context.state.trailersOfFilms.push(response.data.results[i]);
                                        break;
                                    }
                                }
                            })
                    })
                })
        },
        ADD_POPULAR_FILMS_PAGE:(context, list) => context.commit('SET_POPULAR_FILMS_TO_VUEX', list),
        ADD_POPULAR_SERIALS_PAGE:(context, list) => context.commit('SET_POPULAR_SERIALS_TO_VUEX', list),
        CONCAT_POPULAR_FILMS_PAGE:(context, list) => context.commit('CONCAT_POPULAR_FILMS_TO_VUEX', list),
        CONCAT_POPULAR_SERIALS_PAGE:(context, list) => context.commit('CONCAT_POPULAR_SERIALS_TO_VUEX', list),

    },
    mutations: {
        SET_POPULAR_PERSONS_TO_VUEX:(state, popularPersons) => state.popularPersons = popularPersons,
        SET_POPULAR_FILMS_PAGE:state => state.page++,
        SET_POPULAR_FILMS_TO_VUEX:(state, popularFilms) => state.popularFilms = popularFilms,
        SET_POPULAR_SERIALS_TO_VUEX:(state, popularSerials) => state.popularSerials = popularSerials,
        SET_TRENDING_MOVIES_TO_VUEX:(state, trendingMovies) => state.trendingMovies = trendingMovies,
        SET_UPCOMING_FILMS_TO_VUEX:(state, upcomingFilms) => state.upcomingFilms = upcomingFilms,
        SET_TRAILERS_OF_FILMS_TO_VUEX:(state, trailersOfFilms) => state.trailersOfFilms = trailersOfFilms,
        CONCAT_POPULAR_SERIALS_TO_VUEX:(state, popularSerials) => state.popularSerials = state.popularSerials.concat(popularSerials),
        CONCAT_POPULAR_FILMS_TO_VUEX:(state, popularFilms) => state.popularFilms = state.popularFilms.concat(popularFilms),
    },
    state: {
        page: 1,
        person: [],
        popularPersons: [],
        popularFilms: [],
        popularSerials: [],
        upcomingFilms: [],
        upcomingFilmsIds: [],
        trailersOfFilms: [],
        trendingMovies: [],
    },
    getters: {
        POPULAR_PERSONS:state => state.popularPersons,
        POPULAR_FILMS:state => state.popularFilms,
        POPULAR_SERIALS:state => state.popularSerials,
        TRENDING_MOVIES:state => state.trendingMovies,
        UPCOMING_FILMS:state => state.upcomingFilms,
        TRAILERS_OF_FILMS:state => state.trailersOfFilms,
        UPCOMING_FILMS_IDS:state => state.upcomingFilmsIds,
    }
})

